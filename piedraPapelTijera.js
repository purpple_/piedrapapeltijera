function obtenerEleccionJugador(e){
    var eleccion_jugador = {}

    eleccion_jugador['imagen'] = e.target;
    eleccion_jugador['nombre'] = e.target.id;

    return eleccion_jugador;
}

function agregarImagenAlring(imagen){
    $div_eleccion_jugador = document.querySelector('#eleccion_jugador');
    $div_eleccion_maquina = document.querySelector('#eleccion_maquina');

    if($div_eleccion_jugador.querySelector('img') === null){
        $div_eleccion_jugador.appendChild(imagen.cloneNode(true));
    }
    else{
        $img_maquina = $div_eleccion_maquina.querySelector('img');

        src_img = imagen.getAttribute('src');
        alt_img = imagen.getAttribute('alt');

        $img_maquina.setAttribute('src', src_img);
        $img_maquina.setAttribute('alt', alt_img);
    }
}

function eliminarImagenesDelRing(){
    $img_jugador = document.querySelector('#eleccion_jugador img');
    $img_maquina = document.querySelector('#eleccion_maquina img');

    $img_jugador.remove();

    $img_maquina.setAttribute('src','');
    $img_maquina.setAttribute('alt','');
}

function actualizarNumeroRonda(){
    document.querySelector('#ronda').textContent = ronda;
}

function actualizarTurno(){
    turno = document.querySelector('#turno').textContent;

    if(turno == 'jugador'){
        document.querySelector('#turno').textContent = 'maquina';
    }
    else{
        document.querySelector('#turno').textContent = 'jugador';
    }
}

function comenzarOtraPartida(){
    ronda = 0;
    turno = 'jugador';
    //actulizarTurno();
}

function mostrarResultado(resultado){
    var encabezado_con_resultado = document.createElement('h1');
    encabezado_con_resultado.textContent = resultado;

    document.querySelector('#resultado').appendChild(encabezado_con_resultado);
    $('#modalResultados').modal('show')
}

function determinarResultados(puntajes){
    if(puntajes['maquina'] === puntajes['jugador']){
        resultado = 'Han empatado';
    }
    if(puntajes['maquina'] > puntajes['jugador']){
        resultado = 'Ha ganado la máquina :(';
    }
    else{
        resultado = 'Felicidades, ¡¡Has ganado!!';
    }

    return resultado;
}

function comenzarAnimacion(){
    $div_eleccion_maquina = document.querySelector('#eleccion_maquina');

    imagenes_opciones = ['piedra.jpg', 'papel.jpg', 'tijera.jpg'];

    function cambiar_imagen(){
        $img_maquina = $div_eleccion_maquina.querySelector('img');
        var indice_aleatorio = Math.floor(Math.random() * imagenes_opciones.length);

        imagen_aleatoria = imagenes_opciones[indice_aleatorio];
        $img_maquina.setAttribute('src', `imgs/${imagen_aleatoria}`);
    }

    interval_cambio_imagenes = setInterval(cambiar_imagen, 200);

    setTimeout(
        function(){
            clearInterval(interval_cambio_imagenes);
        }, 1000);
}

function comenzarRonda(e){
    eleccion_jugador = obtenerEleccionJugador(e);
    agregarImagenAlring(eleccion_jugador['imagen']);

    comenzarAnimacion();

    setTimeout(
        function(){
            comenzarTurnoMaquina();

    setTimeout(function(){
        actualizarTurno();
    }, 2000)

    puntajes = calcularPuntajes();

    setTimeout(function(){
        eliminarImagenesDelRing();
    }, 2000)

    ronda++;

    actualizarNumeroRonda();
    actualizarTurno();

    if(ronda === 3){
        resultado = determinarResultados(puntajes);
        console.log(resultado);

        setTimeout(function(){
            mostrarResultado(resultado)
        }, 2000);

        setTimeout(function(){
            resetearPartida();
        }, 4000);
    }
        }, 1500);
}

function resetearPartida(){
        ronda = 0;
        turno = 'jugador';

        actualizarNumeroRonda()
        actualizarTurno();

        borrarResultado();
}

function borrarResultado(){
    document.querySelector('#resultado').removeChild('h1');
}

function calcularPuntajes(){
    if(ronda === 0){
        puntaje_maquina = 0;
        puntaje_jugador = 0;
    }

  if(eleccion_jugador['nombre'] == 'piedra'){
        if(eleccion_maquina['nombre'] == 'papel'){
            puntaje_maquina++;
        }
        else if(eleccion_maquina['nombre'] == 'tijera'){
            puntaje_jugador++;
        }
    }

    if(eleccion_jugador['nombre'] == 'papel'){
        if(eleccion_maquina['nombre'] == 'tijera'){
            puntaje_maquina++;
        }
        else if(eleccion_maquina['nombre'] == 'piedra'){
            puntaje_jugador++;
        }
    }

    if(eleccion_jugador['nombre'] == 'tijera'){
        if(eleccion_maquina['nombre'] == 'piedra'){
            puntaje_maquina++;
        }
        else if(eleccion_maquina['nombre'] == 'papel'){
            puntaje_jugador++;
        }
    }

    puntaje = {}

    puntaje['jugador'] = puntaje_jugador;
    puntaje['maquina'] = puntaje_maquina;

    return puntaje;
}

function comenzarTurnoMaquina(){
    eleccion_maquina = elegirOpcionAleatoria();
    agregarImagenAlring(eleccion_maquina['imagen']);
}

function elegirOpcionAleatoria(){
    $opciones = document.querySelectorAll('.opcion');

    var opcion_elegida = {}

    var indice_aleatorio = Math.floor(Math.random() * $opciones.length);

    opcion_elegida['imagen'] = $opciones[indice_aleatorio];
    opcion_elegida['nombre'] = $opciones[indice_aleatorio].getAttribute('id');

    return opcion_elegida;
}

//definiendo valores iniciales
ronda = 0;
turno = 'jugador';

$opciones = document.querySelector('.opciones');
$opciones.onclick = comenzarRonda;

$botonJugarDeNuevo = document.querySelector('#jugarDeNuevo');
$botonJugarDeNuevo.onclick = comenzarOtraPartida();
